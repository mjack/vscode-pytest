# vscode-pytest

Demo: Using pytest with Visual Studio Code

## Setup on Windows
```
> git clone https://codeberg.org/mjack/vscode-pytest.git
> cd vscode-pytest
> python -m venv .venv
> .venv\Scripts\activate
(.venv) > pip install -r requirements.txt
(.venv) > code . &
```